FROM python:3.7-alpine

RUN apk add --no-cache gcc libffi-dev musl-dev openssl-dev

COPY requirements.txt /tmp/requirements.txt
RUN pip install --upgrade pip && pip install -r /tmp/requirements.txt && rm -rf /tmp/requirements.txt

RUN mkdir /app
COPY timerbot.py /app/timerbot.py

ENV TOKEN=DONT_FORGET_TO_GET_A_TELEGRAM_TOKEN
ENV LOG_LEVEL=INFO

CMD python /app/timerbot.py
