# Python Telegram Timerbot

![Logo](./assets/logo.png)

Simple Bot to send timed Telegram messages.
Source code from: [github.com/python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot/blob/v11.1.0/examples/timerbot.py) with few modification *(Removed static values to get them from environment variables)*.

## Create a Telegram Bot

To run this Telegram Bot, you should create a telegram bot talking with [bot father](https://telegram.me/BotFather).
If you don't know how, follow the official guide: [https://core.telegram.org/bots#3-how-do-i-create-a-bot](https://core.telegram.org/bots#3-how-do-i-create-a-bot).

You must get a token and a url.

## Local

### Build locally

You just need to build an OCI Container Image. We will use docker as it's the most common tool.

```bash
$ docker build -t timerbot:local .
```

### Run it locally

If you want to test this bot, create a container from the image built before. Again, we will use docker:

```bash
$ docker run -d --name timerbot -e LOG_LEVEL=DEBUG -e TOKEN=YOUR_TOKEN timerbot:local
```
Environment variables used:

- `LOG_LEVEL`: Bot log level. Default value is set to `INFO`.
- `TOKEN`: Telegram bot token created before.

Once started, you should access the url you created before. Use a telegram client to interact with the Bot

![Help](./assets/help.png)

#### Stop it

Dont forget to stop the bot locally:

```bash
$ docker rm -f timerbot
```

### Deploy to Kubernetes

The `deployment.yml` file contains a kubernetes `deployment` object to deploy this telegram bot inside a kubernetes cluster.
It has a dependency: a `secret`. This `secret` should be named `timerbot` and may exists before deploy the container.

The secret should contains a key with the telegram bot token:

- `TOKEN`: Telegram bot token created before.

Create it::

```bash
$ kubectl create secret generic timerbot --from-literal=TOKEN=${TOKEN}
```

Once created, you can deploy the deployment.

```bash
$ kubectl apply -f deployment.yml
```

After a while, the pod will start and you will be able to use the bot. Just type `/set 60` wait a minute to get the `Beep!`.

![sets](./assets/sets.png)

#### Clean up

To delete the full deployment you should delete the deployment and the secret.

```bash
$ kubectl delete -f deployment.yml
$ kubectl delete secret timerbot
```
